extern crate clap;

use clap::App;
use clap::Arg;
use clap::ArgMatches;

fn main() {
    let matches = App::new("line")
        .arg(
            Arg::with_name("length")
                .short("l")
                .long("length")
                .default_value("80")
                .validator(|v| {
                    usize::from_str_radix(&v, 10)
                        .map_err(|_| "Require a number".to_owned())
                        .map(|_| ())
                }),
        )
        .arg(
            Arg::with_name("text")
                .short("t")
                .long("text")
                .default_value(""),
        )
        .arg(
            Arg::with_name("banner_char")
                .short("c")
                .long("banner-char")
                .default_value("=")
                .validator(|v| {
                    if v.len() != 1 {
                        Err("Expected banner char, not string (i.e. length should be 1)".to_owned())
                    } else {
                        Ok(())
                    }
                }),
        )
        .get_matches();

    println!("{}", make_parameter(&matches).make_banner())
}

struct Parameter {
    pub banner_char: char,
    pub line_width: usize,
    pub line_width_limit: usize,
    pub side_pad_space_length: usize,
    pub side_min_banner_char_length: usize,
    pub ellipse_str: String,
    pub title_text: String,
}

impl Default for Parameter {
    fn default() -> Self {
        Parameter {
            banner_char: '=',
            line_width: 80,
            line_width_limit: 10,
            side_pad_space_length: 1,
            side_min_banner_char_length: 1,
            ellipse_str: "...".to_owned(),
            title_text: String::new(),
        }
    }
}

impl Parameter {
    pub fn make_banner(&self) -> String {
        if self.line_width <= self.line_width_limit || self.title_text.is_empty() {
            // When the output width is too limited,
            // the title text will be ignored
            CharRepeater::new(self.banner_char)
                .take(self.line_width)
                .collect::<String>()
        } else {
            let raw_title_limit = (self.line_width - self.ellipse_str.len()) / 2
                - self.side_min_banner_char_length
                - self.side_pad_space_length;

            let ellipse_str = if raw_title_limit <= self.ellipse_str.len() {
                self.ellipse_str
                    .chars()
                    .take(self.ellipse_str.len() - (self.ellipse_str.len() - raw_title_limit + 1))
                    .collect::<String>()
            } else {
                self.ellipse_str.clone()
            };
            let title_limit = (self.line_width - ellipse_str.len()) / 2
                - self.side_min_banner_char_length
                - self.side_pad_space_length;

            let title_text = if self.title_text.len() > title_limit {
                self.title_text
                    .chars()
                    .take(title_limit - ellipse_str.len())
                    .chain(ellipse_str.chars())
                    .collect::<String>()
            } else {
                self.title_text.to_owned()
            };

            // length of one of (left or right) the banner strip
            let banner_side_strip_length =
                (self.line_width - title_text.len()) / 2 - self.side_pad_space_length;
            let banner_side = CharRepeater::new(self.banner_char)
                .take(banner_side_strip_length)
                .collect::<String>();
            let extra_padding = if (self.line_width - title_text.len()) % 2 == 1 {
                " "
            } else {
                ""
            };

            format!(
                "{} {}{} {}",
                banner_side, title_text, extra_padding, banner_side
            )
        }
    }

    pub fn new() -> Self {
        Parameter::default()
    }
}

fn make_parameter(matches: &ArgMatches) -> Parameter {
    let mut parameter = Parameter::default();

    parameter.line_width = matches
        .value_of("length")
        .and_then(|v| usize::from_str_radix(v, 10).ok())
        .unwrap();
    parameter.banner_char = matches
        .value_of("banner_char")
        .unwrap()
        .chars()
        .next()
        .unwrap();
    parameter.title_text = matches.value_of("text").unwrap().to_owned();
    parameter
}

struct CharRepeater {
    c: char,
}

impl CharRepeater {
    fn new(c: char) -> Self {
        CharRepeater { c }
    }
}

impl Iterator for CharRepeater {
    type Item = char;

    fn next(&mut self) -> Option<<Self as Iterator>::Item> {
        Some(self.c)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_banner_simple() {
        let mut param = Parameter::new();
        param.line_width = 11;
        param.banner_char = '+';
        assert_eq!(param.make_banner(), "+++++++++++")
    }

    #[test]
    fn test_even_banner_with_even_title() {
        let mut param = Parameter::new();
        param.line_width = 20;
        param.title_text = "GG".to_owned();
        assert_eq!(param.make_banner(), "======== GG ========");
    }

    #[test]
    fn test_even_banner_with_odd_title() {
        let mut param = Parameter::new();
        param.line_width = 20;
        param.title_text = "GGG".to_owned();
        assert_eq!(param.make_banner(), "======= GGG  =======");
    }

    #[test]
    fn test_odd_banner_with_even_title() {
        let mut param = Parameter::new();
        param.line_width = 21;
        param.title_text = "GG".to_owned();
        assert_eq!(param.make_banner(), "======== GG  ========");
    }

    #[test]
    fn test_odd_banner_with_odd_title() {
        let mut param = Parameter::new();
        param.line_width = 21;
        param.title_text = "GGG".to_owned();
        assert_eq!(param.make_banner(), "======== GGG ========");
    }

    #[test]
    fn test_mini_banner() {
        let mut param = Parameter::new();
        param.line_width = 5;
        param.title_text = "GG".to_owned();
        assert_eq!(param.make_banner(), "=====");
    }

    #[test]
    fn test_long_title() {
        let mut param = Parameter::new();
        param.line_width = 20;
        param.title_text = "ABCDABCDABCD".to_owned();
        param.ellipse_str = "xxxxx".to_owned();
        assert_eq!(param.make_banner(), "====== ABxxxx ======");
    }
}
